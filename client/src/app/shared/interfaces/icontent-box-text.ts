export interface IContentBoxText {
    "title": string,
    "body": Array<string>
}
