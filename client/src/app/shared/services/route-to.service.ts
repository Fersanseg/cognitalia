import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouteToService {

  constructor(private router:Router) { }

  public route(page: string) {
    console.log("ROUTING TO: ", page)

    switch(page) {
      case "REACTION TEST":
        this.router.navigateByUrl("/reaction-time");
        break;
      case "NUMBER MEMORY":
      case "VERBAL MEMORY":
      case "VISUAL MEMORY":
      case "TYPING SPEED":
      case "STROOP TEST":
        this.router.navigateByUrl("/under-construction");
        break;
      case "LOGIN":
        this.router.navigateByUrl("/login");
        break;
      case "HOME":
        this.router.navigateByUrl("/");
        break;
      default:
        console.log("URL not found");
    }
  }
}
