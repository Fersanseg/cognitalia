export interface IButtonData {
    "caption":string,
    "callback":() => void;
}
