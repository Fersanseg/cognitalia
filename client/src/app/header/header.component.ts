import { Component, OnInit } from '@angular/core';
import { fadeInOut } from '../shared/animations';
import { RouteToService } from '../shared/services/route-to.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [ fadeInOut ]
})
export class HeaderComponent {
  public isAuthCardOpen!: boolean;

  constructor(private routeToService:RouteToService) { }

  public toggleAuthCardOpen() {
    this.isAuthCardOpen = !this.isAuthCardOpen;
  }

  public routeTo(page: string) {
    if (this.isAuthCardOpen) {
      return;
    }

    this.routeToService.route(page);
  }
}
