import { model, Schema } from 'mongoose';
import { ITest } from '../utils/itest';

// Schemas define the data structure of each object
const testSchema = new Schema<ITest>({
  _id: {type: Number},
  title: {
    type: String,
    required: true,
    unique: true,
    dropDups: true,
    uppercase: true,
    trim: true,
    minlength: 3,
  },
  subtitle: { type: String, required: true, trim: true },
  icons: [{type: String}]
}, { versionKey: false });

var handleDuplicateKey = function(err: any, res: any, next: any) {
    if (err.name === "MongoError" && err.code === 11000) {
        next(new Error("Duplicate key error: The document you're trying to add already exists in the database"));
    } else {
        next();
    }
};

testSchema.post('save', handleDuplicateKey);
testSchema.post('update', handleDuplicateKey);
testSchema.post('findOneAndUpdate', handleDuplicateKey);
testSchema.post('insertMany', handleDuplicateKey);

// Models define the actual object (document) instance
export const Test = model<ITest>('Test', testSchema, "TestInfo");

export function GetAllDB() {
    return Test.find({}).sort({_id:"asc"});
}