import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { HomeBoxComponent } from './home-box/home-box.component';
import { WipComponent } from './page-wip/wip.component';
import { ButtonComponent } from './button/button.component';
import { ContentBoxComponent } from './content-box/content-box.component';
import { TstReactionTimeComponent } from './tst-reaction-time/tst-reaction-time.component';
import { AuthCardComponent } from './auth-card/auth-card.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { FrmLabelInputComponent } from './frm-label-input/frm-label-input.component';
import { RegisterFormComponent } from './register-form/register-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    HomeBoxComponent,
    WipComponent,
    ButtonComponent,
    ContentBoxComponent,
    TstReactionTimeComponent,
    AuthCardComponent,
    LoginFormComponent,
    FrmLabelInputComponent,
    RegisterFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
