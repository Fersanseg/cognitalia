import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IButtonData } from '../shared/interfaces/ibutton-data';
import { IContentBoxText } from '../shared/interfaces/icontent-box-text';
import { wipText } from '../shared/utils/sharedDescriptions';

@Component({
  selector: 'app-wip',
  templateUrl: './wip.component.html',
  styleUrls: ['./wip.component.css']
})
export class WipComponent{

  constructor(private router:Router) {}


  ///////////////////////////// CHILD COMPONENT PARAMS
  public homeButtonInfo:IButtonData = {
    caption: "BACK TO HOMEPAGE",
    callback: () => this.router.navigateByUrl("/")
  }
  public text:IContentBoxText = wipText;
}
