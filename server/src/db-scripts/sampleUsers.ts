import * as dotenv from 'dotenv';
import mongoose from 'mongoose';
import { IUser } from '../utils/iuser';
import { dbConnect } from '../database';
import { userModel } from '../models/userAuth.model';
import bcrypt from "bcrypt";

dotenv.config();
const { DB_URI } = process.env;
if (!DB_URI) {
    console.error("No DB_URI variable was found in config.env");
    process.exit(1);
}

function saveUser(model: mongoose.Document<any, any, IUser>):Promise<void> {
    return new Promise((resolve) => {
        model.save()
        .then(() => {
            console.log("User added to database");
            resolve();
        })
        .catch((e) => {
            if (e.name === "MongoServerError" && e.code === 11000) {
                console.log("Duplicate key error: Trying to insert a user that already exists in the database");
            }
        });
    })
}

dbConnect(DB_URI)
    .then(async () => {
        const newUsers:Array<mongoose.Document<any, any, IUser>> = [];
        
        bcrypt.hash("1234", 10, (err: Error, hash: string) => {
            newUsers.push(new userModel({
                email: "testuser@mail.com",
                username: "SampleUser",
                password: hash
            }));
  
            const promises:Array<Promise<void>> = [];
            
            newUsers.forEach((user) => {
                promises.push(saveUser(user));
            });
            
            Promise.all(promises).then(() => {process.exit()});
        })
    });