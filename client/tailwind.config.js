/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/index.html",
    "./src/**/*.{html,ts}"
  ],
  theme: {
    borderWidth: {
      DEFAULT: '1px',
      '0': '0',
      '2': '2px',
      '4': '4px',
      '6': '6px',
      '8': '8px',
      '10': '10px',
      '12': '12px'
    },
    extend: {
      colors: {
        cognitalia: {
          'orange': '#FD5F00',
          'darkBlue': '#0A162E',
          'blue': '#009AFC',
          'white': '#F6F6E9'
        }
      },
      scale: {
        '70': '.75',
        '60': '.60'
      },
      width: {
        '13': '3.25rem'
      },
      keyframes: {
        smallBounce: {
          '50%': {transform: 'translateY(-5%)'},
          '0%, 100%': {transform: 'translateY(0%)'}
        }
      },
      animation: {
        smallBounce: 'smallBounce .2s ease-in-out'
      }
    },
  },
  plugins: [],
}
