import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestsService } from './tests.service';
import { ITest } from '../interfaces/itest';

////////////////////////////////////////////////////////////////////////////////
//  https://testing-angular.com/testing-services/#call-the-method-under-test  //
////////////////////////////////////////////////////////////////////////////////

describe('TestsService', () => {
  let service: TestsService;
  let controller: HttpTestingController;
  const URL = 'http://localhost:5200/testInfo';
  const mockResponse = [
    {
      _id: 0,
      title: "TITLE 1",
      subtitle: "Subtitle of test 1",
      icons: ["./assets/mockIcon.jpg"]
    },
    {
      _id: 1,
      title: "TITLE 2",
      subtitle: "Subtitle of test 2",
      icons: ["./assets/mockIcon.jpg", "./assets/mockIcon.jpg"]
    }
  ];
  let actualResponse: ITest[] | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TestsService]
    });
    service = TestBed.inject(TestsService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch the tests info', () => {
    
    service.GetAllTestsInfo().subscribe((info:ITest[]) => {
      /* Expecting the result here is not ideal because if the code behind the
      test is broken, the observable won't emit and "expect" will not be called,
      but the test will appear to pass. An undefined response is assigned to the
      actual response, and the real expected response is tested later */
      actualResponse = info;
    })
    
    const request = controller.expectOne(URL);
    request.flush(mockResponse);

    expect(actualResponse).toEqual(mockResponse);
  })
});