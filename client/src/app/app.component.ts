import { Component } from '@angular/core';
import { ChildrenOutletContexts, NavigationStart, Router, RouterEvent } from '@angular/router';
import { slideInAnimation } from './shared/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [slideInAnimation]
})
export class AppComponent {
  public title:string = 'Cognitalia';

  constructor( private context:ChildrenOutletContexts ) { }

  public getRouteAnimationData() {
    return this.context.getContext('primary')?.route?.snapshot?.data?.['animation'];
  }

  public onRouting(event: Event) {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}
