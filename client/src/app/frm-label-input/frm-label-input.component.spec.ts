import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrmLabelInputComponent } from './frm-label-input.component';

describe('FrmLabelInputComponent', () => {
  let component: FrmLabelInputComponent;
  let fixture: ComponentFixture<FrmLabelInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrmLabelInputComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FrmLabelInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
