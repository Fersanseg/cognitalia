import { GetUserByLogin, SaveNewUser } from "../models/userAuth.model";
import { IUser } from "../utils/iuser";
import { credsError } from "../utils/constants";
import bcrypt from "bcrypt";

class UserAuthBL {
  Login(user: IUser) {
    return new Promise((resolve, reject) => {
      try {
        GetUserByLogin(user.email).then((resp) => {
          if (!resp) {
            reject(credsError); // Email doesn't exist in DB
            return;
          }

          const recoveredPassword = resp.password;
          bcrypt.compare(user.password, recoveredPassword, (err: Error, result: boolean) => {
            if (err) {
              reject(err);
            }

            if (!result) {
              reject(credsError); // Email exists in DB, but password doesn't match
            }
            else {
              resolve(resp); // Send back the response if login is OK
            }
          })
        })
      } catch (e) {
        reject(e);
      }
    });
  }

  SetNewUser(user: IUser) {
    const hashedPass = UserAuthBL.hashPassword(user.password);

    return hashedPass.then((r: string) => {
      const encryptedUser: IUser = { ...user, password: r };

      return new Promise((resolve, reject) => {
        SaveNewUser(encryptedUser)
          .then((resp) => resolve(resp))
          .catch((err) => {
            if (err.code == 11000) {
              const code = 400;
              let message = "Error: Invalid credentials";
              if (err.keyPattern.hasOwnProperty('email')) {
                message = "This email already exists. Try a different email"
              }
              else if (err.keyPattern.hasOwnProperty('username')) {
                message = "This username already exists. Try a different username"
              }
              
              reject({code: code, message: message})
            }
          });
      });
    });
  }

  private static hashPassword(password: string) {
    return new Promise((resolve, reject) => {
      bcrypt.hash(password, 10, (err: Error, hash: string) => {
        if (err) {
          reject(err);
        }
        resolve(hash);
      });
    });
  }
}

export const userAuthBL = new UserAuthBL();
