export interface IInputMetadata {
    type: string,
    name: string,
    id: string
}
