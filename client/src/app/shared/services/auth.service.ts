import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUserData } from '../interfaces/iuser-data';
import { environment as env } from 'src/environments/environment';
import { shareReplay } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public loginUser(user: IUserData) {
    return this.http.post<string>(`${env.BACK_URL}/auth/login`, user, httpOptions).pipe(
      shareReplay()
    )
  }

  public registerUser(user: IUserData) {
    return this.http.post<string>(`${env.BACK_URL}/auth/register`, user, httpOptions).pipe(
      shareReplay()
    )
  }
}
