import { Request, Response } from "express";
import { testInfoBL } from "../services/testInfoBL";

/* The controller calls a service function, which handles business logic, and
handles the HTTP response appropriately. */
class TestInfoController {    
    GetAllTestsInfo(req: Request, res: Response) {
        testInfoBL.GetAll()
            .then(r => {
                res.status(200).json(r);
            })
            .catch(e => {
                console.error(e.message);
                res.sendStatus(500);
            })
    }
}

export const testInfoController = new TestInfoController();