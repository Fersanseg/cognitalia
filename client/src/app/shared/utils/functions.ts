/**
 * @param n1 Lower range of the generated number
 * @param n2 Upper range of the generated number
 * @param n3 Length in digits that the generated number will have
 * @returns A random number in the specified range, with the specified length
 */
export function randomNumber(n1: number, n2: number, n3: number): number {
    let min: number;
    let max: number;

    if (n1 == n2) {
        return n1;
    }
    else if (n1<n2) {
        min = n1;
        max = n2;
    } 
    else {
        min = n2;
        max = n1;
    }

    let length: string = "1";
    for (let i=1; i<n3; i++) {
        length += "0";
    }

    return (Math.random() * (max-min) + min) * parseInt(length);
}