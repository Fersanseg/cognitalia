export const statesToColors: object = {
    "init": "bg-slate-200",
    "wait": "bg-blue-500",
    "answerable": "bg-cognitalia-orange",
    "correct": "bg-green-500",
    "failed": "bg-red-500",
    "finished": "bg-slate-200"
}