import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { TestsService } from '../shared/services/tests.service';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  const mockResponse = [
    { title: "TITLE 1", subtitle: "Subtitle of test 1" },
    { title: "TITLE 2", subtitle: "Subtitle of test 2" },
    { title: "TITLE 3", subtitle: "Subtitle of test 3" },

  ];
  let GetAllTestsInfoSpy;

  beforeEach(async () => {
    // Create a fake service with a spy on the testing method
    const testsService = jasmine.createSpyObj('TestsService', ['GetAllTestsInfo']);
    GetAllTestsInfoSpy = testsService.GetAllTestsInfo.and.returnValue(of(mockResponse));

    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      providers: [{provide: TestsService, useValue: testsService}]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the service to fetch test info', () => {
    expect(component.tests).toBeFalsy();
    fixture.detectChanges();
    expect(component.tests).toEqual(mockResponse);
  });
});