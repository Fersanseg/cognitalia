import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public footerRedirect(id:string):void {
    switch(id) {
      case "gitlab":
        window.open("https://gitlab.com/Fersanseg/cognitalia", "_blank");
    }    
  }
}
