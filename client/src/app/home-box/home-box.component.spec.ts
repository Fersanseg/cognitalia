import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ITest } from "../shared/interfaces/itest";
import { HomeBoxComponent } from "./home-box.component";

describe('HomeBoxComponent', () => {
  let component: HomeBoxComponent;
  let fixture: ComponentFixture<HomeBoxComponent>;
  let testEl: any;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeBoxComponent ]
    });

    fixture = TestBed.createComponent(HomeBoxComponent);
    component = fixture.componentInstance;
    testEl = fixture.nativeElement;
    component.test = mockInput;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should get an ITest input and print title, subtitle and icons', () => {
    expect(testEl.querySelector('h1').innerText).toContain('test title');
    expect(testEl.querySelector('span').innerText).toContain('test subtitle');
  })

  const mockInput: ITest = {
    _id: 0,
    title: "test title",
    subtitle: "test subtitle",
    icons: ["test icons"]
  };
})