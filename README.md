# COGNITALIA
A website made as a learning exercise in Angular, Express and noSQL databases (MongoDB), using TypeScript as the main programming language in every part of the application.

This website was born as a means to unite the two fields which I pursued after high school: psychology and programming.
In this site you will find a host of tests/games that measure various cognitive or dexterity abilities, such as reaction time (how fast can you respond after you're presented with a stimulus) or typing speed (how many words per minute you can type correctly). 

## DISCLAIMER
The games found in this website are just that, only games. They aren't, in no way, shape or form, a substitute for professional psychiatric evaluation and I never intended for them to be. The results you get on these games aren't representative or indicative of anything: a bad result could be due to a lot of factors, ranging from suboptimal code, to me not trying to emulate real psychological tests, or even possible distractions when playing the games. There are a lot of uncontrolled variables that I can't control that would be properly accounted for in a professional setting, with real, professional tests and trained doctors.
