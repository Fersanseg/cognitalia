import { model, Schema } from "mongoose";
import { IUser } from "../utils/iuser";

const userSchema = new Schema<IUser>({
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
  },
  username: { 
    type: String, 
    required: true, 
    unique: true, 
    trim: true 
  },
  password: { type: String, required: true, trim: true },
});

export const userModel = model<IUser>("User", userSchema, "Users");

export function GetUserByLogin(login: string) {
  return userModel.findOne({ email: login });
}

export function SaveNewUser(userData: IUser) {
  return userModel.create(userData);
}