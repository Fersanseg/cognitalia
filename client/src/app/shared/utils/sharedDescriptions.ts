import { IContentBoxText } from '../interfaces/icontent-box-text';

///////////////////////////// REACTION TIME
export const rtIntroText: IContentBoxText = {
    title: `REACTION TIME`,
    body: [
        `In this test, you will need to closely observe your screen and 
        click in the test area as quickly as you can once the red box turns 
        green. But be careful: if you click too soon, that attempt will be 
        discarded.`,
        `The test measures the time from the point the stimulus happens, 
        until your brain proccesses it, sends the signal to click to your 
        hand, and your muscles react.`,
        `The fastest reaction time ever recorded is 120ms, whereas the 
        average time is around 250ms, and its not rare to get a bit less 
        than 200ms or a bit more than 300ms. Remember: the lower the time, 
        the better. It means you can react faster.`
    ],
};

///////////////////////////// WIP
export const wipText: IContentBoxText = {
    title: `UNDER CONSTRUCTION`,
    body: [
      `Oops! This test is currently being developed, and therefore it's not 
      available yet. Sorry!`,
      `In the meantime, why don't you try some of our other cognitive 
      tests?`
    ]
}