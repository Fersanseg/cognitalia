import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { IContentBoxText } from '../shared/interfaces/icontent-box-text';
import { IHeading } from '../shared/interfaces/iheading';
import { RTService } from '../shared/services/rt.service';
import { rtIntroText } from '../shared/utils/sharedDescriptions';
import { statesToColors } from '../shared/utils/testStatesToColors';

@Component({
  selector: 'tst-reaction-time',
  templateUrl: './tst-reaction-time.component.html',
  styleUrls: ['./tst-reaction-time.component.css'],
})
export class TstReactionTimeComponent implements OnInit, OnDestroy {
  public testsFinishedCount$!: Observable<number>;
  public latestScore$!: Observable<number|null>;
  public testInstructions$!: Observable<IHeading>;
  public averageReactionTime$!: Observable<string>;

  public testState!: string;
  public testResults: string = "";
  public resetTestCallback!: () => void;

  private _stateSubscription!: Subscription;
  private _testResultsSubscription!: Subscription;
  private _testsCount: number = 0;

  constructor(private reactionTimeService: RTService) {}

  ngOnInit(): void {
    this.testInstructions$ = this.reactionTimeService.getInstructions();
    this.latestScore$ = this.reactionTimeService.getLatestScore();
    this.testsFinishedCount$ = this.reactionTimeService.getTestFinishedCount();

    this._stateSubscription = this.reactionTimeService
      .getCurrentState()
      .subscribe((s) => {
        this.testState = statesToColors[s as keyof typeof statesToColors];
      });
    
    this._testResultsSubscription = this.latestScore$.subscribe(sc => {
      if (sc != null) {
        this.testResults += sc.toString() + "ms";
        this._testsCount++;

        if (this._testsCount < 5) {
          this.testResults += ", ";
        }

        if (this._testsCount == 5) {
          this.averageReactionTime$ = this.reactionTimeService.getAverageReactionTime();
        }
      }
    })

    this.resetTestCallback = (): void => {
      this.reactionTimeService.resetTest();
      this.testResults = "";
      this._testsCount = 0;
    }
  }

  ngOnDestroy(): void {
    this._stateSubscription.unsubscribe();
    this._testResultsSubscription.unsubscribe();
  }

  public testBox_Click() {
    this.reactionTimeService.handleState();
  }

  ///////////////////////////// CHILD COMPONENT PARAMS
  public testIntro: IContentBoxText = rtIntroText;
}
