import * as dotenv from 'dotenv';
import mongoose from 'mongoose';
import { dbConnect } from '../database';

dotenv.config();
const { DB_URI } = process.env;
if (!DB_URI) {
    console.error("No DB_URI variable was found in config.env");
    process.exit(1);
}

dbConnect(DB_URI)
    .then(() => {
        mongoose.connection.db.dropDatabase()
            .then(() => {
                console.log("Database dropped")
                process.exit();
            });
    });

