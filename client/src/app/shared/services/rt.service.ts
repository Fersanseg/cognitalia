import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { IHeading } from '../interfaces/iheading';
import { randomNumber } from '../utils/functions';

@Injectable({
  providedIn: 'root'
})
export class RTService {
  private _stateSubject!: BehaviorSubject<string>;
  private _instructionsSubject!: BehaviorSubject<IHeading>;
  private _latestResultSubject!: BehaviorSubject<number|null>;
  private _testsFinishedCountSubject!: BehaviorSubject<number>;

  private _initialState: string = "init"
  private _initialInstructions: IHeading = {
    title: "When this box changes from blue to orange, click it as fast as you can!",
    subtitle: "Click here whenever you're ready to start"
  }

  private _clickAllowed: boolean = false;
  private _timeoutId!: ReturnType<typeof setTimeout>;
  private _startTime!: number;
  private _clickTime!: number;
  private _results: Array<number> = [];

  constructor() {
    this._stateSubject = new BehaviorSubject<string>(this._initialState);
    this._instructionsSubject = new BehaviorSubject<IHeading>(this._initialInstructions);
    this._latestResultSubject = new BehaviorSubject<number|null>(null);
    this._testsFinishedCountSubject = new BehaviorSubject<number>(0);
  }

  public getCurrentState(): Observable<string> {
    return this._stateSubject.asObservable();
  }

  public getInstructions(): Observable<IHeading> {
    return this._instructionsSubject.asObservable();
  }

  public getLatestScore(): Observable<number|null> {
    return this._latestResultSubject.asObservable();
  }

  public getTestFinishedCount(): Observable<number> {
    return this._testsFinishedCountSubject.asObservable();
  }

  public getAverageReactionTime(): Observable<string> {
    let avg: number = 0;
    if (this._results.length > 0) {
      avg = (this._results.reduce((a: number, b: number): number => a+b))/5;
    }
    
    return of(avg.toString()+"ms");
  }

  public handleState(): void {
    switch (this._stateSubject.value) {
      case "correct":
      case "init":
      case "failed":
        this._clickAllowed = false;
        this._stateSubject.next("wait");
        this._instructionsSubject.next({
          title: "Wait for it..."
        })

        this._timeoutId = setTimeout(() => {
          this._clickAllowed = true;
          this.handleState();
        }, randomNumber(1, 2, 4));
        break;

      case "wait":
        if (this._clickAllowed) {
          this._startTime = Date.now();
          this._stateSubject.next("answerable");
          this._instructionsSubject.next({
            title: "NOW!"
          })
        }
        else { // Click prematurely
          if (this._timeoutId) {
            clearTimeout(this._timeoutId);
          }
          
          this._stateSubject.next("failed");
          this._instructionsSubject.next({
            title: "Too fast!",
            subtitle: "You have to wait until this box turns orange. Click here to try again!"
          })
        }
        break;
      
      case "answerable":
        this._clickTime = Date.now();
        this.handleResults();

        if (this._testsFinishedCountSubject.value < 5) {
          this._stateSubject.next("correct");
          this._instructionsSubject.next({
            title: `Your time: ${this._results[this._results.length -1]}ms`,
            subtitle: "Click again to continue"
          })
          this._latestResultSubject.next(this._results[this._results.length -1]);
        }
        else {
          this._stateSubject.next("finished");
          this._instructionsSubject.next({
            title: `Your time: ${this._results[this._results.length -1]}`,
            subtitle: "You're done! Thanks for participating. Check your _results down below"
          })
          this._latestResultSubject.next(this._results[this._results.length -1]);
        }
        break;
    }
  }

  public resetTest() {
    this._stateSubject.next("init");
    this._instructionsSubject.next(this._initialInstructions);
    this._latestResultSubject.next(null);
    this._testsFinishedCountSubject.next(0);

    if (this._timeoutId) {
      clearTimeout(this._timeoutId);
    }

  }

  private handleResults() {
    this._results.push(this._clickTime - this._startTime);
    this._clickTime = this._startTime = 0;

    this._testsFinishedCountSubject.next(this._testsFinishedCountSubject.value + 1);
  }
}
