import { animate, animateChild, group, query, state, style, transition, trigger } from "@angular/animations";

const optional = {optional: true};

export const slideInAnimation = trigger('routeAnimations', [
  transition('* <=> *', [
    style({position: 'relative'}),
    query(':enter, :leave', [
      style({
        position: 'absolute',
        width: '100%',
        height: '100%'
      })
    ], optional),
    query(':enter', [
      style({opacity: '0'})
    ], optional),
    query(':leave', animateChild(), optional),
    group([
      query(':leave', [
        animate('200ms ease-in-out', style({opacity: '0'}))
      ], optional),
      query(':enter', [
        animate('300ms 200ms ease-in-out', style({opacity: '100%'}))
      ], optional)
    ])
  ])
])

export const authCardAnimation = trigger('authCardAnimation', [
  state("closed", style({
    transform: "translateX(100%)"
  })),
  state("open", style({
    transform: "translateX(0)"
  })),
  transition("closed => open", [animate("0.55s ease-out")]),
  transition("open => closed", [animate("0.55s ease-in")])
])

export const fadeInOut = trigger('fadeInOut', [
  transition(':enter', [
    style({ opacity: 0 }),
    animate('0.2s', style({ opacity: 1 })),
  ]),
  transition(':leave', [
    animate('0.2s', style({ opacity: 0 }))
  ])
])