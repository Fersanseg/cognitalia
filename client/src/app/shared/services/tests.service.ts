import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment as env } from 'src/environments/environment';
import { ITest } from '../interfaces/itest';

@Injectable({
  providedIn: 'root'
})
export class TestsService {

  constructor(private http:HttpClient) { }

  public GetAllTestsInfo():Observable<ITest[]> {
    return this.http.get<ITest[]>(`${env.BACK_URL}/testInfo`);
  }
}
