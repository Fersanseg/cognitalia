import mongoose from 'mongoose';

export async function dbConnect(uri:string):Promise<any> {
    return mongoose.connect(uri)
        .then(() => console.log('Connected to database'))
        .catch((err) => console.log(err));
}