import * as dotenv from 'dotenv';
import mongoose from 'mongoose';
import { ITest } from '../utils/itest';
import { dbConnect } from '../database';
import { Test as testModel } from '../models/testInfo.model';

dotenv.config();
const { DB_URI } = process.env;
if (!DB_URI) {
    console.error("No DB_URI variable was found in config.env");
    process.exit(1);
}

let idCount:number = 0;

function saveDocument(model: mongoose.Document<any, any, ITest>):Promise<void> {
    return new Promise((resolve) => {
        model.save()
        .then(() => {
            console.log("Document added to the database with ID", model.id);
            resolve();
        })
        .catch((e) => {
            if (e.name === "MongoServerError" && e.code === 11000) {
                console.log("Duplicate key error: The document with ID", model.id,"already exists in the database");
            }
        });
    })
}

dbConnect(DB_URI)
    .then(async () => {
        const newDocuments:Array<mongoose.Document<any, any, ITest>> = [];
        
        // New tests require an icon to be added to the "assets" folder in the client project
        newDocuments.push(new testModel({
            _id: idCount++,
            title: "Reaction test",
            subtitle: "How quickly can you react?",
            icons: ["./assets/clock-regular.svg"]
        }));
        newDocuments.push(new testModel({
            _id: idCount++,
            title: "Number memory",
            subtitle: "How many digits can you remember at once?",
            icons: ["./assets/1-solid.svg", "./assets/2-solid.svg", "./assets/3-solid.svg"]
        }));
        newDocuments.push(new testModel({
            _id: idCount++,
            title: "Verbal memory",
            subtitle: "Remember as many words as you can",
            icons: ["./assets/h-solid.svg", "./assets/i-solid.svg", "./assets/exclamation-solid.svg"]
        }));
        newDocuments.push(new testModel({
            _id: idCount++,
            title: "Visual memory",
            subtitle: "Memorize a checkered board that gets progressively bigger",
            icons: ["./assets/table-cells-solid.svg"]
        }));
        newDocuments.push(new testModel({
            _id: idCount++,
            title: "Typing speed",
            subtitle: "Measure how many words per minute you can type",
            icons: ["./assets/keyboard-solid.svg"]
        }));

        newDocuments.push(new testModel({
            _id: idCount++,
            title: "Stroop test",
            subtitle: "Can you successfully ignore conflictive stimuli?",
            icons: ["./assets/palette-solid.svg"]
        }));

        const promises:Array<Promise<void>> = [];

        newDocuments.forEach((doc) => {
            promises.push(saveDocument(doc));
        });

        Promise.all(promises).then(() => {process.exit()});
    });