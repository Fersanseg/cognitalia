import { TestBed } from '@angular/core/testing';

import { RTService } from './rt.service';

describe('RTService', () => {
  let service: RTService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RTService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
