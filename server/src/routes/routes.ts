import { Application } from "express";
import { testInfoController } from "../controllers/testInfo.controller";
import { authController } from "../controllers/auth.controller";

/* Routes only redirect the request to the appropriate controller and delegates
any HTTP logic handling to it */
export class AppRoutes {
  public route(app: Application) {
    app.get("/testInfo", testInfoController.GetAllTestsInfo);
    app.post("/auth/register", authController.SetNewUser);
    app.post("/auth/login", authController.Login);
  }
}
