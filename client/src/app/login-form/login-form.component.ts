import { AfterViewInit, Component, EventEmitter, OnDestroy, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FrmLabelInputComponent } from '../frm-label-input/frm-label-input.component';
import { IInputMetadata } from '../shared/interfaces/iinput-metadata';
import { AuthService } from '../shared/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements AfterViewInit {
  @ViewChild('emailInput') emailInput!: FrmLabelInputComponent;
  @ViewChild('pwInput') pwInput!: FrmLabelInputComponent;

  @Output() changeModeEvent: EventEmitter<string> = new EventEmitter<string>();

  private emailControl!: FormControl;
  private pwControl!: FormControl;

  public invalidFormSubmitted: boolean = false;

  public emailInputMetadata: IInputMetadata = {
    type: "email",
    name: "emailLoginInput",
    id: "emailLoginInput"
  }
  public pwInputMetadata: IInputMetadata = {
    type: "password",
    name: "pwLoginInput",
    id: "pwLoginInput" 
  }

  constructor(private auth: AuthService) { }

  ngAfterViewInit(): void {
    this.emailControl = this.emailInput.inputControl;
    this.pwControl = this.pwInput.inputControl;
  }

  public changeMode() {
    this.changeModeEvent.emit("register");
  }

  public submit() {
    const validInputs = this.emailInput.inputControl.valid && this.pwInput.inputControl.valid;
    
    if (!validInputs) {
      this.invalidFormSubmitted = true;
    }

    this.auth.loginUser({
      email: this.emailControl.value,
      password: this.pwControl.value
    }).subscribe({
      next: r => console.log("logged in ", r),
      error: err => console.error(`An error happened during login.\n${err.error}`, err)
    });
  }

  public keyPress(ev: KeyboardEvent) {
    if (ev.key == "Enter") {
      this.submit();
    }
  }
}
