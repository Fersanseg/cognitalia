import { Component, Input, OnInit } from '@angular/core';
import { IButtonData } from '../shared/interfaces/ibutton-data';
import { IContentBoxText } from '../shared/interfaces/icontent-box-text';

@Component({
  selector: 'content-box',
  templateUrl: './content-box.component.html',
  styleUrls: ['./content-box.component.css']
})
export class ContentBoxComponent implements OnInit {
  @Input() public text?:IContentBoxText
  @Input() public buttonData?:IButtonData;

  constructor() { }

  ngOnInit(): void {
  }

}
