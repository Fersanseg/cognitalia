export interface ITest {
    _id: number,
    title: string,
    subtitle: string,
    icons: Array<string>
}
