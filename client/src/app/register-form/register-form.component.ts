import { AfterViewInit, Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FrmLabelInputComponent } from '../frm-label-input/frm-label-input.component';
import { IInputMetadata } from '../shared/interfaces/iinput-metadata';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements AfterViewInit {
  @ViewChild('emailInput') emailInput!: FrmLabelInputComponent;
  @ViewChild('usernameInput') usernameInput!: FrmLabelInputComponent;
  @ViewChild('pwInput') pwInput!: FrmLabelInputComponent;
  @ViewChild('pwInput2') pwInput2!: FrmLabelInputComponent;

  @Output() changeModeEvent: EventEmitter<string> = new EventEmitter<string>();

  private emailControl!: FormControl;
  private usernameControl!: FormControl;
  private pwControl!: FormControl;
  private pw2Control!: FormControl;

  public invalidFormSubmitted: boolean = false;
  public matchingPasswords: boolean = true;

  public emailInputMetadata: IInputMetadata = {
    type: "email",
    name: "emailRegisterInput",
    id: "emailRegisterInput"
  }
  public usernameInputMetadata: IInputMetadata = {
    type: "text",
    name: "usernameRegisterInput",
    id: "usernameRegisterInput"
  }
  public pwInputMetadata: IInputMetadata = {
    type: "password",
    name: "pwRegisterInput",
    id: "pwRegisterInput" 
  }
  public pwInputMetadata2: IInputMetadata = {
    type: "password",
    name: "pwRegisterInput2",
    id: "pwRegisterInput2" 
  }

  constructor(private auth: AuthService) { }

  ngAfterViewInit(): void {
    this.emailControl = this.emailInput.inputControl;
    this.usernameControl = this.usernameInput.inputControl;
    this.pwControl = this.pwInput.inputControl;
    this.pw2Control = this.pwInput2.inputControl;
  }

  public changeMode() {
    this.changeModeEvent.emit("login");
  }

  public submit() {
    const validForm = this.validateForm();

    if (!validForm) {
      this.invalidFormSubmitted = true;
    } 
    else {
      this.auth.registerUser({
        email: this.emailControl.value,
        username: this.usernameControl.value.toLowerCase(),
        password: this.pwControl.value
      })
      .subscribe({
        next: r => console.log("registered user ", r),
        error: err => console.error(`An error happened during registration.\n${err.error}`, err)
      })
    }
  }

  public keyPress(ev: KeyboardEvent) {
    if (ev.key == "Enter") {
      this.submit();
    }
  }

  private validateForm(): boolean {
    const matchingPasswords = this.pwControl.value === this.pw2Control.value;

    const validInputs = matchingPasswords
      && this.emailControl.valid 
      && this.usernameControl.valid
      && this.pwControl.valid
      && this.pw2Control.valid;
      
    this.matchingPasswords = matchingPasswords;

    return validInputs;
  }

}
