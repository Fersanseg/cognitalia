import { Component, Input, OnInit } from '@angular/core';
import { ITest } from '../shared/interfaces/itest';

@Component({
  selector: 'home-box',
  templateUrl: './home-box.component.html',
  styleUrls: ['./home-box.component.css']
})
export class HomeBoxComponent implements OnInit {
  @Input() test!: ITest;

  constructor() { }

  ngOnInit(): void {
  }

}
