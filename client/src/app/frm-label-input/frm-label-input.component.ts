import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, ValidatorFn, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IInputMetadata } from '../shared/interfaces/iinput-metadata';

@Component({
  selector: 'frm-label-input',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './frm-label-input.component.html',
  styleUrls: ['./frm-label-input.component.css']
})
export class FrmLabelInputComponent implements OnInit, OnDestroy {
  @Input() label: string = "";
  @Input() inputMetadata: IInputMetadata = { // Initial metadata values
    type: "",
    name: "",
    id: "input-"+(((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
  };
  @Input() mode!: string;
  @Input() secondLoginAttempt: boolean = false;

  public inputControl: FormControl = new FormControl('');
  
  private _value!: number|string;
  private _valueSubcription!: Subscription;

  constructor() {}

  ngOnInit(): void {
    this._valueSubcription = this.inputControl.valueChanges.subscribe(v => this._value = v);
    this.inputControl.setValidators(this.getValidator());
  }

  ngOnDestroy(): void {
    this._valueSubcription.unsubscribe();
  }

  public get value() {
    return this._value;
  }

  private getValidator(): ValidatorFn | ValidatorFn[] | null {
    switch (this.mode) {
      case "email":
        return [Validators.pattern("[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}"), Validators.required];
      default: 
        return [Validators.required, Validators.minLength(4)];
      }
  }
}
