import { Component, OnInit } from '@angular/core';
import { IButtonData } from '../shared/interfaces/ibutton-data';
import { IContentBoxText } from '../shared/interfaces/icontent-box-text';
import { ITest } from '../shared/interfaces/itest';
import { RouteToService } from '../shared/services/route-to.service';
import { TestsService } from '../shared/services/tests.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public tests!:ITest[];

  constructor (
    private testsService:TestsService, 
    private routeToService:RouteToService
  ) { }

  ngOnInit(): void {
    this.testsService.GetAllTestsInfo().subscribe(resp => this.tests = resp);
  }
  
  public routeTo(page: string) {
    this.routeToService.route(page);
  }

  ///////////////////////////// CHILD COMPONENT PARAMS
  public homepageButtonData:IButtonData = {
    caption: "BACK TO HOMEPAGE",
    callback: () => this.routeTo("HOME")
  }
  public errorContentBoxText:IContentBoxText = {
    title: "UH OH...",
    body: [
      `Something went wrong when trying to load the content. Try reloading 
      the page. If the issue persists, we will work to fix the issue as soon
      as possible.`,
      `Sorry for the inconvenience!`
    ]
  }
}
