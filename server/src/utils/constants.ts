import { IError } from "./ierror";

export const credsError: IError = {
  message: "Invalid email or password",
  code: 400
}