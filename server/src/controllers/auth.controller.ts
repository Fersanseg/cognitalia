import { Request, Response } from "express";
import { IError } from "../utils/ierror";
import { userAuthBL } from "../services/userAuthBL";

class AuthController {
  
  Login(req: Request, res: Response) {
    if (req.body.email && req.body.password) {
      userAuthBL.Login(req.body)
        .then((data) => res.status(200).json(data))
        .catch((error: IError) => {res.status(error.code).send(error.message)});
    }
  }

  SetNewUser(req: Request, res: Response) {
    if (req.body.email && req.body.username && req.body.password) {
      userAuthBL.SetNewUser(req.body)
        .then((data) => res.status(200).json(data))
        .catch((error: IError) => res.status(error.code).send(error.message));
    } else {
      res.status(400).send("Some of the data is missing");
    }
  }
}

export const authController = new AuthController();