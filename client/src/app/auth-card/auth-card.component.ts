import { Component, Input } from '@angular/core';
import { authCardAnimation } from '../shared/animations';

@Component({
  selector: 'auth-card',
  templateUrl: './auth-card.component.html',
  styleUrls: ['./auth-card.component.css'],
  animations: [ authCardAnimation ]
})
export class AuthCardComponent {
  @Input() isOpen!: boolean;
  public authMode: "login" | "register" = "login";

  public setMode(mode: string) {
    if (mode != "login" && mode != "register") {
      return;
    }

    this.authMode = mode;
  }
}
