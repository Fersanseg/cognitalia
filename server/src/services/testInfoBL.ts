import { GetAllDB } from '../models/testInfo.model';

class TestInfoBL {
    async GetAll() {
        return new Promise((resolve, reject) => {
            try {
                GetAllDB()
                    .then(response => {
                        resolve(response);
                    });
            } catch (e) {
                reject(e);
            }
        })
    }
}

export const testInfoBL = new TestInfoBL();