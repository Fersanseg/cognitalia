import { TestBed } from '@angular/core/testing';

import { RouteToService } from './route-to.service';

describe('RouteToService', () => {
  let service: RouteToService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RouteToService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
