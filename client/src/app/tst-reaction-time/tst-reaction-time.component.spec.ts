import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TstReactionTimeComponent } from './tst-reaction-time.component';

describe('TstReactionTimeComponent', () => {
  let component: TstReactionTimeComponent;
  let fixture: ComponentFixture<TstReactionTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TstReactionTimeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TstReactionTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
