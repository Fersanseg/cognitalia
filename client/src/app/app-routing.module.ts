import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TstReactionTimeComponent } from './tst-reaction-time/tst-reaction-time.component';
import { WipComponent } from './page-wip/wip.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'reaction-time', component: TstReactionTimeComponent, data: { 
    animation: true 
  } },
  { path: 'under-construction', component: WipComponent, data: { 
    animation: true 
  } },
  { path: '**', redirectTo: '/under-construction'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
