import * as dotenv from 'dotenv';
import cors from "cors";
import express from "express";
import { dbConnect } from './database';
import { AppRoutes } from './routes/routes';


dotenv.config();

const { DB_URI } = process.env;

if (!DB_URI) {
    console.error("No DB_URI variable was found in config.env");
    process.exit(1);
}

const appRoutes = new AppRoutes();

dbConnect(DB_URI)
    .then(() => {
        const app = express();
        app.use(cors({
            methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
            origin: '*'
        }));
        
        app.use(express.json());
        app.listen(5200, () => {
            console.log('Server listening at port 5200');
        });
        appRoutes.route(app);
    })
    .catch(err => console.error(err));